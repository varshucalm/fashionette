package com.skava.framework.action;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.jsoup.Jsoup;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.Point;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.framework.reporting.BaseClass;
import com.framework.reporting.ExtentTestManager;

import com.galenframework.api.Galen;
import com.galenframework.reports.model.LayoutReport;
import com.relevantcodes.extentreports.LogStatus;
import com.skava.frameworkutils.Constants;
import com.skava.frameworkutils.loggerUtils;

import net.lightbody.bmp.BrowserMobProxy;
import net.lightbody.bmp.BrowserMobProxyServer;
import net.lightbody.bmp.client.ClientUtil;
import net.lightbody.bmp.core.har.Har;
import net.lightbody.bmp.proxy.CaptureType;

public class SeleniumActionEngine implements ActionEngine 
{
	private WebDriver driver;
	private static ChromeDriverService service = null;
	public static final String saucelabAPI_Web = "http://"+BaseClass.properties.getProperty("SaucelabUserName")+":"+BaseClass.properties.getProperty("SaucelabAccessKey")+"@ondemand.saucelabs.com:80/wd/hub";
	public static final String saucelabAPI_US = "https://us1.appium.testobject.com/wd/hub";
	public static final String saucelabAPI_EU = "https://eu1.appium.testobject.com/wd/hub";
	ArrayList<String> tabs;
	private static int port=System.getProperty("port")==null?8081:Integer.parseInt(System.getProperty("port"));
	String screenshotEmailPath = BaseClass.currentRunReportPath+"\\Screenshots\\";
	
	
	// wait config details
	public static int Wait_Time;
	//private static int port=8081;
	
	WebDriverWait wait;
	
	public BrowserMobProxy proxy = new BrowserMobProxyServer();
	
	public SeleniumActionEngine(int browserType) 
	{
		init(browserType);
	}

	private void init(int browserType) 
	{
		String currentThreadName[] = Thread.currentThread().getStackTrace()[5].getClassName().split("\\.");
        String testCaseName = currentThreadName[(currentThreadName.length-1)];
		String strBrowserName=(String) BaseClass.json.getJSONArray(testCaseName).get(2);
		String strBrowserVersion=(String) BaseClass.json.getJSONArray(testCaseName).get(3);
		String strDeviceName=(String) BaseClass.json.getJSONArray(testCaseName).get(5);
		String strDeviceOS=(String) BaseClass.json.getJSONArray(testCaseName).get(6);
		
//		Wait_Time=BaseClass.properties.containsKey("Wait_Time")?Integer.valueOf(BaseClass.properties.getProperty("Wait_Time"))>=0?Integer.valueOf(BaseClass.properties.getProperty("Wait_Time")):0:0;
		Wait_Time=BaseClass.properties.containsKey("Wait_Time")?Integer.valueOf(BaseClass.properties.getProperty("Wait_Time")):0;
		
		System.out.println("Wait Config: "+Wait_Time);
		DesiredCapabilities caps;
		
		switch (browserType) 
		{
			case Constants.LOCAL_BROWSER_FIREFOX:
				System.setProperty("webdriver.gecko.driver", BaseClass.UserDir+BaseClass.properties.getProperty("DriversPath")+"geckodriver.exe");
				FirefoxOptions fOptions=new FirefoxOptions();
				if(BaseClass.properties.getProperty("RunWithHeadless").equalsIgnoreCase("Yes"))
				{
					fOptions.addArguments("--headless");
				}
				
				if(BaseClass.properties.getProperty("RunWithProxy").equalsIgnoreCase("YES"))
                {

					// start the proxy
					//System.out.println(port);
					proxy.start(port);
					port++;
					 // get the Selenium proxy object
				    Proxy seleniumProxy = ClientUtil.createSeleniumProxy(proxy);
				    fOptions.setCapability(CapabilityType.PROXY, seleniumProxy);
                }
				
				this.driver = new FirefoxDriver(fOptions);
				
				break;
			
			case Constants.LOCAL_BROWSER_CHROME:
				
				if("Linux".equalsIgnoreCase(System.getProperty("os.name")))
	        	{	
				   	System.setProperty("webdriver.chrome.driver", BaseClass.UserDir+BaseClass.properties.getProperty("DriversPath")+"chromedriverlnx");
	        	}
			    else
			    {
			    	System.setProperty("webdriver.chrome.driver", BaseClass.UserDir+BaseClass.properties.getProperty("DriversPath")+"chromedriver.exe");
			    }
				
				//System.setProperty("webdriver.chrome.logfile", "D:\\chromedriver.log");
				//System.setProperty("webdriver.chrome.verboseLogging", "true");
				
				Map<String, Object> prefs = new LinkedHashMap<>();
                prefs.put("credentials_enable_service", Boolean.valueOf(false));
                prefs.put("profile.password_manager_enabled", Boolean.valueOf(false));
				ChromeOptions options = new ChromeOptions();
				//options.setExperimentalOption("useAutomationExtension", false);
				options.setExperimentalOption("excludeSwitches",Collections.singletonList("enable-automation"));
				options.addArguments("--disable-gpu");
				options.addArguments("--disable-dev-shm-usage");
				options.addArguments("detach=false");
				options.setPageLoadStrategy(PageLoadStrategy.NORMAL);
				options.addArguments("--disable-infobars");
				options.addArguments("enable-automation");
				options.addArguments("--disable-browser-side-navigation");
				options.addArguments("--disable-extensions");
				//options.addArguments("--silent");
				options.addArguments("--no-sandbox");
				options.addArguments("--window-size=1366,768");
				options.addArguments("--start-maximized");
				options.setExperimentalOption("prefs", prefs);
				//options.addArguments("--port=8888");
				System.out.println(BaseClass.properties.getProperty("RunWithHeadless"));
                if(BaseClass.properties.getProperty("RunWithHeadless").equalsIgnoreCase("Yes"))
				{
                	options.addArguments("--headless");
				}
                
                if(BaseClass.properties.getProperty("RunWithProxy").equalsIgnoreCase("YES"))
                {
	             // start the proxy
					//System.out.println(port);
					//proxy.start(port);
					proxy.start();
					port++;
					 // get the Selenium proxy object
				    Proxy seleniumProxy = ClientUtil.createSeleniumProxy(proxy);
				    options.setCapability(CapabilityType.PROXY, seleniumProxy);
                }
        		
				if (service == null) {
					synchronized (SeleniumActionEngine.class) {
						int attempts = 0;
						while (attempts < 3) {
							ChromeDriverService.Builder serviceBuilder = new ChromeDriverService.Builder()
									.usingAnyFreePort();
							service = serviceBuilder.build();
							try {
								service.start();
								attempts = 3;
							} catch (IOException ioe) {
								System.out.println("Error: Unable to start chrome dirver service");
								attempts++;
							}
						}
					}
				}
                this.driver=new RemoteWebDriver(service.getUrl(), options);
                break;
				
			case Constants.LOCAL_BROWSER_IE:
				System.setProperty("webdriver.ie.driver", BaseClass.UserDir+BaseClass.properties.getProperty("DriversPath")+"IEDriverServer.exe");
				InternetExplorerOptions ieOption=new InternetExplorerOptions();
				ieOption.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
				
				this.driver = new InternetExplorerDriver(ieOption);
				
				break;
				
			case Constants.LOCAL_BROWSER_SAFARI:
				this.driver = new SafariDriver();
				
				break;
				
			case Constants.LOCAL_BROWSER_EDGE:
				System.setProperty("webdriver.edge.driver", BaseClass.UserDir+BaseClass.properties.getProperty("DriversPath")+"MicrosoftEdgeDriver.exe");
				this.driver = new EdgeDriver();
				
				break;
				
			case Constants.SAUCE_DESKTOP_BROWSER_CHROME:
				caps = DesiredCapabilities.chrome();
				caps.setCapability("platform", strDeviceOS.trim());
				caps.setCapability("version", strBrowserVersion.trim());
				caps.setCapability("name", BaseClass.properties.getProperty("ProjectName")+"_"+testCaseName);
				try 
				{
					driver = new RemoteWebDriver(new java.net.URL(saucelabAPI_Web), caps);
				} 
				catch (MalformedURLException e) 
				{
					loggerUtils.stackTracePrint(e);
				}
				break;	
				
			case Constants.SAUCE_DESKTOP_BROWSER_FIREFOX:
				caps = DesiredCapabilities.firefox();
				caps.setCapability("platform", strDeviceOS.trim());
				caps.setCapability("version", strBrowserVersion.trim());
				caps.setCapability("name", BaseClass.properties.getProperty("ProjectName")+"_"+testCaseName);
				try 
				{
					driver = new RemoteWebDriver(new java.net.URL(saucelabAPI_Web), caps);
				} 
				catch (MalformedURLException e) 
				{
					loggerUtils.stackTracePrint(e);
				}
				break;
				
			case Constants.SAUCE_DESKTOP_BROWSER_IE:
				caps = DesiredCapabilities.internetExplorer();
				caps.setCapability("platform", strDeviceOS.trim());
				caps.setCapability("version", strBrowserVersion.trim());
				caps.setCapability("name", BaseClass.properties.getProperty("ProjectName")+"_"+testCaseName);
				try 
				{
					driver = new RemoteWebDriver(new java.net.URL(saucelabAPI_Web), caps);
				} 
				catch (MalformedURLException e) 
				{
					loggerUtils.stackTracePrint(e);
				}
				break;
				
			case Constants.SAUCE_DESKTOP_BROWSER_EDGE:
				caps = DesiredCapabilities.edge();
				caps.setCapability("platform", strDeviceOS.trim());
				caps.setCapability("version", strBrowserVersion.trim());
				caps.setCapability("name", BaseClass.properties.getProperty("ProjectName")+"_"+testCaseName);
				try 
				{
					driver = new RemoteWebDriver(new java.net.URL(saucelabAPI_Web), caps);
				} 
				catch (MalformedURLException e) 
				{
					loggerUtils.stackTracePrint(e);
				}
				break;
				
			case Constants.SAUCE_DESKTOP_BROWSER_SAFARI:
				caps = DesiredCapabilities.safari();
				caps.setCapability("platform", strDeviceOS.trim());
				caps.setCapability("version", strBrowserVersion.trim());
				caps.setCapability("name", BaseClass.properties.getProperty("ProjectName")+"_"+testCaseName);
				try 
				{
					driver = new RemoteWebDriver(new java.net.URL(saucelabAPI_Web), caps);
				} 
				catch (MalformedURLException e) 
				{
					loggerUtils.stackTracePrint(e);
				}
				break;
			
			case Constants.SAUCE_MOBILE_ANDROID_BROWSER_CHROME:
				caps = new DesiredCapabilities();
				caps.setCapability("testobjectApiKey", BaseClass.properties.getProperty("TestobjectApiKey"));
				caps.setCapability("platformName", "Android");
				caps.setCapability("platformVersion", strDeviceOS.replace("Android ", ""));
				caps.setCapability("deviceName", strDeviceName);
				caps.setCapability("privateDevicesOnly", "false	");
				caps.setCapability("deviceType", "mobile");
				caps.setCapability("testobject_app_id", "1");
				caps.setCapability("browserName", "Chrome");
				caps.setCapability("name", BaseClass.properties.getProperty("ProjectName")+"_"+testCaseName);
				caps.setCapability("testobject_session_creation_timeout", "900000");
				caps.setCapability("noReset", "false");
				caps.setCapability("appiumVersion", "1.7.1");
				try 
				{
					if(BaseClass.properties.getProperty("RealDeviceDataCenter").equalsIgnoreCase("US"))
						driver = new RemoteWebDriver(new java.net.URL(saucelabAPI_US), caps);
					else
						driver = new RemoteWebDriver(new java.net.URL(saucelabAPI_EU), caps);
				}
				catch (MalformedURLException e) 
				{
					loggerUtils.stackTracePrint(e);
				}   
				break;
				
			case Constants.SAUCE_MOBILE_EMULATOR_ANDROID_BROWSER_CHROME:
				caps = DesiredCapabilities.android();
				caps.setCapability("appiumVersion", "1.7.1");
				caps.setCapability("deviceName",strDeviceName);
				caps.setCapability("deviceOrientation", "portrait");
				caps.setCapability("deviceType", "mobile");
				caps.setCapability("browserName", strBrowserName.trim());
				caps.setCapability("platformVersion",strDeviceOS.replace("Android ", ""));
				caps.setCapability("platformName","Android");
				caps.setCapability("name", BaseClass.properties.getProperty("ProjectName")+"_"+testCaseName);
				try 
				{
					driver = new RemoteWebDriver(new java.net.URL(saucelabAPI_Web), caps);
				} 
				catch (MalformedURLException e) 
				{
					loggerUtils.stackTracePrint(e);
				}
				break;
				  
			case Constants.SAUCE_MOBILE_IOS_BROWSER_SAFARI:
				caps = new DesiredCapabilities();
				caps.setCapability("testobjectApiKey", BaseClass.properties.getProperty("TestobjectApiKey"));
				caps.setCapability("platformName", "iOS");
				caps.setCapability("platformVersion", strDeviceOS.replace("iOS ", ""));
				caps.setCapability("deviceOrientation", "portrait");
				caps.setCapability("deviceName", strDeviceName);
				caps.setCapability("testobject_app_id", "1");
				caps.setCapability("browserName", strBrowserName.trim());
				caps.setCapability("name", BaseClass.properties.getProperty("ProjectName")+"_"+testCaseName);
				caps.setCapability("testobject_session_creation_timeout", "900000");
				caps.setCapability("appiumVersion", "1.7.1");
				try 
				{
					if(BaseClass.properties.getProperty("RealDeviceDataCenter").equalsIgnoreCase("US"))
						driver = new RemoteWebDriver(new java.net.URL(saucelabAPI_US), caps);
					else
						driver = new RemoteWebDriver(new java.net.URL(saucelabAPI_EU), caps);
				}
				catch (MalformedURLException e) 
				{
					loggerUtils.stackTracePrint(e);
				}   
				break;
				
			case Constants.SAUCE_MOBILE_SIMULATOR_IOS_BROWSER_SAFARI:
				caps = DesiredCapabilities.iphone();
				caps.setCapability("appiumVersion", "1.7.1");
				caps.setCapability("deviceName",strDeviceName);
				caps.setCapability("deviceOrientation", "portrait");
				caps.setCapability("platformVersion",strDeviceOS.replace("iOS ", ""));
				caps.setCapability("platformName", "iOS");
				caps.setCapability("browserName", strBrowserName.trim());
				caps.setCapability("name", BaseClass.properties.getProperty("ProjectName")+"_"+testCaseName);
				try 
				{
					driver = new RemoteWebDriver(new java.net.URL(saucelabAPI_Web), caps);
				} 
				catch (MalformedURLException e) 
				{
					loggerUtils.stackTracePrint(e);
				}
				break;
				
			case Constants.SAUCE_TABLET_EMULATOR_ANDROID_BROWSER_CHROME:
				caps = DesiredCapabilities.android();
				caps.setCapability("appiumVersion", "1.7.1");
				caps.setCapability("deviceName",strDeviceName);
				caps.setCapability("deviceOrientation", "portrait");
				caps.setCapability("deviceType", "tablet");
				caps.setCapability("browserName", strBrowserName.trim());
				caps.setCapability("platformVersion",strDeviceOS.replace("Android ", ""));
				caps.setCapability("platformName","Android");
				caps.setCapability("name", BaseClass.properties.getProperty("ProjectName")+"_"+testCaseName);
				try 
				{
					driver = new RemoteWebDriver(new java.net.URL(saucelabAPI_Web), caps);
				} 
				catch (MalformedURLException e) 
				{
					loggerUtils.stackTracePrint(e);
				}
				break;
				
			case Constants.SAUCE_TABLET_ANDROID_BROWSER_CHROME:
				caps = new DesiredCapabilities();
				caps.setCapability("testobjectApiKey", BaseClass.properties.getProperty("TestobjectApiKey"));
				caps.setCapability("platformName", "Android");
				caps.setCapability("platformVersion", strDeviceOS.replace("Android ", ""));
				caps.setCapability("deviceName", strDeviceName);
				caps.setCapability("privateDevicesOnly", "false	");
				caps.setCapability("deviceType", "tablet");
				caps.setCapability("testobject_app_id", "1");
				caps.setCapability("browserName", "Chrome");
				caps.setCapability("name", BaseClass.properties.getProperty("ProjectName")+"_"+testCaseName);
				caps.setCapability("testobject_session_creation_timeout", "900000");
				caps.setCapability("noReset", "false");
				caps.setCapability("appiumVersion", "1.7.1");
				try 
				{
					if(BaseClass.properties.getProperty("RealDeviceDataCenter").equalsIgnoreCase("US"))
						driver = new RemoteWebDriver(new java.net.URL(saucelabAPI_US), caps);
					else
						driver = new RemoteWebDriver(new java.net.URL(saucelabAPI_EU), caps);
				}
				catch (MalformedURLException e) 
				{
					loggerUtils.stackTracePrint(e);
				}   
				break;
				
			case Constants.SAUCE_TABLET_IOS_BROWSER_SAFARI:
				caps = new DesiredCapabilities();
				caps.setCapability("testobjectApiKey", BaseClass.properties.getProperty("TestobjectApiKey"));
				caps.setCapability("platformName", "iOS");
				caps.setCapability("platformVersion", strDeviceOS.replace("iOS ", ""));
				caps.setCapability("deviceOrientation", "portrait");
				caps.setCapability("deviceName", strDeviceName);
				caps.setCapability("testobject_app_id", "1");
				caps.setCapability("browserName", strBrowserName.trim());
				caps.setCapability("name", BaseClass.properties.getProperty("ProjectName")+"_"+testCaseName);
				caps.setCapability("testobject_session_creation_timeout", "900000");
				caps.setCapability("appiumVersion", "1.7.1");
				try 
				{
					if(BaseClass.properties.getProperty("RealDeviceDataCenter").equalsIgnoreCase("US"))
						driver = new RemoteWebDriver(new java.net.URL(saucelabAPI_US), caps);
					else
						driver = new RemoteWebDriver(new java.net.URL(saucelabAPI_EU), caps);
				}
				catch (MalformedURLException e) 
				{
					loggerUtils.stackTracePrint(e);
				}   
				break;
				
			case Constants.SAUCE_TABLET_SIMULATOR_IOS_BROWSER_SAFARI:
				caps = DesiredCapabilities.ipad();
				caps.setCapability("appiumVersion", "1.7.1");
				caps.setCapability("deviceName",strDeviceName);
				caps.setCapability("deviceOrientation", "portrait");
				caps.setCapability("platformVersion",strDeviceOS.replace("iOS ", ""));
				caps.setCapability("platformName", "iOS");
				caps.setCapability("browserName", strBrowserName.trim());
				caps.setCapability("name", BaseClass.properties.getProperty("ProjectName")+"_"+testCaseName);
				try 
				{
					driver = new RemoteWebDriver(new java.net.URL(saucelabAPI_Web), caps);
				} 
				catch (MalformedURLException e) 
				{
					loggerUtils.stackTracePrint(e);
				}
				break;
				
			case Constants.LOCAL_Emulator_IOS_Chrome:
				if("Linux".equalsIgnoreCase(System.getProperty("os.name")))
	        	{				
				   	System.setProperty("webdriver.chrome.driver", BaseClass.UserDir+BaseClass.properties.getProperty("DriversPath")+"chromedriver");
	        	}
			    else
			    {
			    	System.setProperty("webdriver.chrome.driver", BaseClass.UserDir+BaseClass.properties.getProperty("DriversPath")+"chromedriver.exe");
			    }
                
                Map<String, String> mobileEmulation = new HashMap<>();
                mobileEmulation.put("deviceName", "iPhone 8 Plus");
                ChromeOptions chromeOptions = new ChromeOptions();
                chromeOptions.setExperimentalOption("mobileEmulation", mobileEmulation);
                chromeOptions.addArguments("disable-infobars");
                if(BaseClass.properties.getProperty("RunWithHeadless").equalsIgnoreCase("Yes"))
				{
                	chromeOptions.addArguments("--headless");
				}
                
                if(BaseClass.properties.getProperty("RunWithProxy").equalsIgnoreCase("YES"))
                {
	                //start the proxy
					//System.out.println(port);
					//proxy.start(port);
					proxy.start();
					port++;
					 // get the Selenium proxy object
				    Proxy seleniumProxy = ClientUtil.createSeleniumProxy(proxy);
				    chromeOptions.setCapability(CapabilityType.PROXY, seleniumProxy);
                }
                
                this.driver = new ChromeDriver(chromeOptions);
                this.driver.manage().deleteAllCookies();
                
                break;
				
			default:
				//throw new InvalidBrowserTypeException();
			break;
		}
		
//		wait= new WebDriverWait(driver, Wait_Time);
		wait= new WebDriverWait(this.driver, Wait_Time);
	}


	@Override
	public boolean enterText(By textField, String textValue,String elementName)
	{
		Boolean valid = false;
		 try
		 {
			Boolean visibility = explicitWaitforVisibility(textField,elementName);
			if( visibility && !textValue.equals(""))
			{
				WebElement element = this.driver.findElement(textField);
				element.clear();
				element.sendKeys(textValue);
				valid = true;
				loggerUtils.passLog(elementName+" is entered in the field");
			}
		 }
		 catch(Exception e)
		 {
			 //BaseClass.processErrorCodes(e,elementName+" ("+textField.toString()+")");
			 loggerUtils.stackTracePrint(e);
			 loggerUtils.failLog(elementName+"is not present");
			 //takeScreenShot(elementName);
		 }
		 return valid;
	}
	
	@Override
	public boolean appendText(By textField, String textValue,String elementName)
	{
		Boolean valid = false;
		 try
		 {
			Boolean visibility = explicitWaitforVisibility(textField,elementName);
			if( visibility && !textValue.equals(""))
			{
				WebElement element = this.driver.findElement(textField);
				element.sendKeys(textValue);
				valid = true;
			}
		 }
		 catch(Exception e)
		 {
			 //BaseClass.processErrorCodes(e,elementName+" ("+textField.toString()+")");
			 loggerUtils.stackTracePrint(e);
			 //takeScreenShot(elementName);
		 }
		 return valid;
	}
	
	@Override
	public int getSize(By path,String elementName)
	{
		return driver.findElements(path).size();
	}

	@Override
	public boolean clickElement(By buttonField,String elementName)
	{
		Boolean valid = false;
		try
		{	
			if(explicitWaitforClickable(buttonField,elementName))
			{
				this.driver.findElement(buttonField).click();
				valid = true;
				loggerUtils.passLog(elementName+" is clicked");
			}
		
		}
		catch(Exception e)
		{
			//BaseClass.processErrorCodes(e,elementName+" ("+buttonField.toString()+")");
			loggerUtils.stackTracePrint(e);
			loggerUtils.failLog(elementName+"is not present");
			//takeScreenShot(elementName);
		}
		return valid;
	}

	@Override
	public boolean isElementPresent(By path,String elementName)
	{
		return !this.driver.findElements(path).isEmpty();
	}	 

	@Override
	public boolean compareElementText(By path,String expectedText,String elementName)
	{
		Boolean valid = false;
			try
			{
				if(explicitWaitforVisibility(path,elementName))
				{
					scrollToElement(path, elementName);
					String currentText = getText(path, elementName).trim();
					expectedText=Jsoup.parse(expectedText).text();
					if(currentText.equals(expectedText))
					{
						loggerUtils.passLog(elementName+" is verified");
						valid = true;
					}
					else
					{
						//Logging
						loggerUtils.failLog(elementName+" is mismatched");
					}
				}
			}
			catch(Exception e)
			{
				//BaseClass.processErrorCodes(e,elementName+" ("+path.toString()+")");
				loggerUtils.stackTracePrint(e);
				loggerUtils.failLog(elementName+" is not present");
				//takeScreenShot(elementName);
			}
			return valid;
	}

	@Override
	public boolean explicitWaitforVisibility(By path,String elementName) 
	{
		boolean toRet = false;
		int attempts = 0;
		while (attempts < 3) {
			try {
				
				wait.ignoring(StaleElementReferenceException.class)
						.until(ExpectedConditions.visibilityOfElementLocated(path));
				toRet = true;
				attempts = 3;
			} catch (StaleElementReferenceException e) {
				attempts++;
			} catch (Exception e) {
				loggerUtils.stackTracePrint(e);
////				ExtentTestManager.getTest().log(LogStatus.INFO, getStackTrace(e));
////				ExtentTestManager.getTest().log(LogStatus.INFO,
//						elementName + " xpath " + path.toString() + " is not present");
//				loggerUtils.failLog(elementName + " is not present");
				attempts = 3;
				// takeScreenShot(elementName);
			}
		}
		return toRet;
	}
	
	@Override
	public boolean explicitWaitforVisibilityWithTime(By path,int seconds,String elementName) 
	{
		boolean toRet = false;
		int attempts = 0;
		while (attempts < 3) {
			try {
				
				WebDriverWait wait= new WebDriverWait(this.driver, seconds);
				wait.ignoring(StaleElementReferenceException.class)
						.until(ExpectedConditions.visibilityOfElementLocated(path));
				toRet = true;
				attempts = 3;
			} catch (StaleElementReferenceException e) {
				attempts++;
			} catch (Exception e) {
				loggerUtils.stackTracePrint(e);
////				ExtentTestManager.getTest().log(LogStatus.INFO, getStackTrace(e));
////				ExtentTestManager.getTest().log(LogStatus.INFO,
//						elementName + " xpath " + path.toString() + " is not present");
//				loggerUtils.failLog(elementName + " is not present");
				attempts = 3;
				// takeScreenShot(elementName);
			}
		}
		return toRet;
	}

	@Override
	public boolean isElementDisplayed(By path,String elementName)
	{
		Boolean valid = false;
			try
			{
				if(explicitWaitforVisibility(path,elementName))
				{
					WebElement element = this.driver.findElement(path);
					valid = element.isDisplayed();
					if(valid)
						loggerUtils.passLog(elementName+" is displayed");
					else
						loggerUtils.failLog(elementName+" is not displayed");
				}
			}
			catch(Exception e)
			{
				//BaseClass.processErrorCodes(e,elementName+" ("+path.toString()+")");
				loggerUtils.stackTracePrint(e);
				loggerUtils.failLog(elementName+" is not present");
				//takeScreenShot(elementName);
			}
			return valid;
	}

	@Override
	public boolean isElementNotDisplayed(By path,String elementName)
	{
		Boolean valid = true;
			if(isElementPresent(path, elementName))
			{
				WebElement element = this.driver.findElement(path);
				valid = !element.isDisplayed();
				if(!valid)
				{
					//takeScreenShot(elementName);
				}
			}
		return valid;
	}

	@Override
	public boolean isElementEnabled(By path,String elementName)
	{
		Boolean valid = false;
		try
		{
			if(explicitWaitforVisibility(path,elementName))
			{
				WebElement element = this.driver.findElement(path);
				valid = element.isEnabled();
				if(valid)
					loggerUtils.passLog(elementName+" is enabled");
				else
					loggerUtils.failLog(elementName+" is not enabled");
			}
		}
		catch (Exception e)
		{
			//BaseClass.processErrorCodes(e,elementName+" ("+path.toString()+")");
			loggerUtils.stackTracePrint(e);
			loggerUtils.failLog(elementName+" is not present");
			//takeScreenShot(elementName);
		}
		return valid;
	}

	@Override
	public String getText(By path,String elementName)
	{
		String text = "";
			try
			{
				if(explicitWaitforVisibility(path,elementName))
				{
					WebElement element = this.driver.findElement(path);
					text = element.getText();
				}
			}
			catch(Exception e)
			{
				//BaseClass.processErrorCodes(e,elementName+" ("+path.toString()+")");
				loggerUtils.stackTracePrint(e);
				loggerUtils.failLog(elementName+" is not present");
				//takeScreenShot(elementName);
			}
			return text;
	}

	@Override
	public String getElementAttribute(By path,String attrName,String elementName)
	{
		String text = "";
			try
			{
				if(explicitWaitforVisibility(path,elementName))
				{
					WebElement element = this.driver.findElement(path);
					text = element.getAttribute(attrName);
				}
			}
			catch(Exception e)
			{
				//BaseClass.processErrorCodes(e,elementName+" ("+path.toString()+")");
				loggerUtils.stackTracePrint(e);
				loggerUtils.failLog(elementName+" is not present");
				//takeScreenShot(elementName);
			}
			return text;
	}
	
	@Override
	public Boolean isElementAttributePresent(By path,String attrName,String elementName){
	    Boolean result = false;
	    try {
	    	WebElement element = this.driver.findElement(path);
	        String value = element.getAttribute(attrName);
	        if (value != null){
	            result = true;
	        }
	    } catch (Exception e) {}

	    return result;
	}

	@Override
	public boolean compareElementAttribute(By path,String attrName,String expectedText,String elementName)
	{
		Boolean valid = false;
			try
			{
				if(explicitWaitforVisibility(path,elementName))
				{
					WebElement element = this.driver.findElement(path);
					if(element.getAttribute(attrName).equals(expectedText))
					{
						valid = true;
						loggerUtils.passLog(elementName+" is verified");
					}
					else
					{
						//Logging
						loggerUtils.failLog(elementName+" is mismatched");
					}
				}
			}
			catch(Exception e)
			{
				//BaseClass.processErrorCodes(e,elementName+" ("+path.toString()+")");
				loggerUtils.stackTracePrint(e);
				loggerUtils.failLog(elementName+" is not present");
				//takeScreenShot(elementName);
			}
		return valid;
	}

	@Override
    public void takeScreenShot(String elementName)
    {
      try
      {
    	  DateFormat dateFormat = new SimpleDateFormat("h_m_s");
		  Date date = new Date();
    	  TakesScreenshot scrShot =((TakesScreenshot)this.driver);
    	  File sourceFile=scrShot.getScreenshotAs(OutputType.FILE);
    	  
    	  FileUtils.copyFile(sourceFile,new File(BaseClass.currentRunReportPath+"/"+elementName+"_"+dateFormat.format(date)+".png"));
      }
      catch (Exception e)
      {
    	  //BaseClass.processErrorCodes(e,elementName);
    	  loggerUtils.stackTracePrint(e);
      }
    }

	@Override
	public int generateRandomNumber(int limit)
	{
		Random rand = new Random();
		return rand.nextInt(limit);
	}
	
	@Override
	public int generateRandomNumberWithLimit(int maximum,int minimum)
	{
		Random rand = new Random();
		return rand.nextInt(maximum) + minimum;
	}
	
	@Override
	public boolean scrollToElement(By path,String elementName)
	{
		Boolean valid = false;
		try
		{
			WebElement element = this.driver.findElement(path);
			((JavascriptExecutor) this.driver).executeScript("arguments[0].scrollIntoView();", element);
			valid = true;
			loggerUtils.passLog("scrolled to "+elementName);
		}
		catch(Exception e)
		{
			//BaseClass.processErrorCodes(e,elementName+" ("+path.toString()+")");
			loggerUtils.stackTracePrint(e);
			loggerUtils.failLog(elementName+" is not present");
			//takeScreenShot(elementName);
			valid = false;
		}
		return valid;
	}

	@Override
	public void maximizeBrowser() 
	{
		driver.manage().window().maximize();
	}

	@Override
	public void navigateToUrl(String url) 
	{
		driver.get(url);
		loggerUtils.passLog(url+" is opened");
	}
	
	@Override
	public boolean enterTextAndSubmit(By path,String value,String elementName)
	{
		 boolean valid=false;
		 try
		 {
			 if(explicitWaitforVisibility(path,elementName) && !value.equals(""))
			 {
				 WebElement element = driver.findElement(path);
				 element.clear();
				 element.sendKeys(value);
				 element.sendKeys(Keys.ENTER);
				 valid = true;
				 loggerUtils.passLog(value+" is entered in "+elementName);
			 }
		 }
		 catch(Exception e)
		 {
			 //BaseClass.processErrorCodes(e,elementName+" ("+path.toString()+")");
			 loggerUtils.stackTracePrint(e);
			 loggerUtils.failLog(elementName+"is not present");
			 //takeScreenShot(elementName);
		 }
		return valid;
	}

	@Override
	public WebElement findElement(By txtusername) 
	{
		return driver.findElement(txtusername);
	}

	@Override
	public void quit() 
	{
		/*driver.close();
		
		try 
		{
			Thread.sleep(1000);
		} 
		catch (InterruptedException e) 
		{
			e.printStackTrace();
		}
		
        driver.quit();*/
	}
	
	@Override
	public void cleanUp() {
		service.stop();
	}

	@Override
	public String getCurrentUrl()
	{
		return driver.getCurrentUrl();
	}
	
	@Override
	public boolean selectByIndex(By path,int indexField,String elementName)
	{
		Boolean valid = false;
		try
		{	
				WebElement element = this.driver.findElement(path);
				Select dropdown= new Select(element);
				dropdown.selectByIndex(indexField);
				valid = true;
		}
		catch(Exception e)
		{
			//BaseClass.processErrorCodes(e,elementName+" ("+path.toString()+")");
			loggerUtils.stackTracePrint(e);
			//takeScreenShot(elementName);
		}
		return valid;
	}
	
	@Override
	public boolean isElementSelected(By path, String elementName)
	{
		boolean valid=false;
		try{
			
			if(explicitWaitforVisibility(path, elementName))
			{
				WebElement element = this.driver.findElement(path);
				valid=element.isSelected();
			}
		}
		catch(Exception e)
		{
			//BaseClass.processErrorCodes(e,elementName+" ("+path.toString()+")");
			e.printStackTrace();
			//takeScreenShot(elementName);
		}
		
		return valid;
	}
	
	@Override
	public boolean isElementNotSelected(By path, String elementName)
	{
		boolean valid=true;
		try{
			
			if(explicitWaitforVisibility(path, elementName))
			{
				WebElement element = this.driver.findElement(path);
				valid=element.isSelected();
			}
		}
		catch(Exception e)
		{
			//BaseClass.processErrorCodes(e,elementName+" ("+path.toString()+")");
			e.printStackTrace();
			//takeScreenShot(elementName);
		}
		
		return valid;
	}
	
	@Override
	public boolean explicitWaitforInVisibility(By path,String elementName)
	{
		try
		{
			
	        wait.ignoring(StaleElementReferenceException.class)
			.until(ExpectedConditions.invisibilityOfElementLocated(path));
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			//takeScreenShot(elementName);
			return false;
		}
	}
	
	@Override
	public Boolean tabKey(By buttonField,String elementName)
	{
		Boolean valid = false;
		try
		{	
			if(explicitWaitforVisibility(buttonField,elementName))
			{
				WebElement element = this.driver.findElement(buttonField);
				element.sendKeys(Keys.TAB);
				valid = true;
			}
		}
		catch(Exception e)
		{
			//BaseClass.processErrorCodes(e,elementName+" ("+buttonField.toString()+")");
			loggerUtils.stackTracePrint(e);
			//takeScreenShot(elementName);
		}
		return valid;
	}
	
	@Override
	public boolean waitForPageLoad(int secs)
	{
		boolean valid=false;
		try
		{
			
			driver.manage().timeouts().pageLoadTimeout(secs, TimeUnit.SECONDS);
			valid=true;
		}
		catch(Exception e)
		{
			loggerUtils.stackTracePrint(e);
		}
		return valid;
	}

	@Override
	public boolean moveToElementAndClick(By path, String elementName)
	{
		Boolean valid = false;
		try
		{
			WebElement element = driver.findElement(path);
			Actions act = new Actions(this.driver);
			act.moveToElement(element).click().build().perform();
			valid = true;
		}
		catch(Exception e)
		{
			//BaseClass.processErrorCodes(e,elementName+" ("+path.toString()+")");
			loggerUtils.stackTracePrint(e);
            //takeScreenShot(elementName);
            
    	}
		return valid;
	}
	
	@Override
	public boolean jsClickElement(By buttonField,String elementName)
	{
		boolean toRet = false;
		int attempts = 0;
		while (attempts < 3) {
			try {
				WebElement element = driver.findElement(buttonField);
				 JavascriptExecutor executor = (JavascriptExecutor) driver;
				 executor.executeScript("arguments[0].click();", element);
				 toRet = true;
				attempts = 3;
				loggerUtils.passLog(elementName+" is clicked");
				if(!waitForJSandJQueryToLoad()) {
	        ExtentTestManager.getTest().log(LogStatus.ERROR, "page load failed");
				}
			} catch (StaleElementReferenceException e) {
//				ExtentTestManager.getTest().log(LogStatus.INFO, getStackTrace(e));
				System.out.println(getStackTrace(e));
				attempts++;
			} catch (Exception e) {
				loggerUtils.stackTracePrint(e);
//				ExtentTestManager.getTest().log(LogStatus.INFO, getStackTrace(e));
//				ExtentTestManager.getTest().log(LogStatus.INFO,elementName+" xpath "+buttonField.toString()+" is not present");
				System.out.println(getStackTrace(e));
				System.out.println(elementName+" xpath "+buttonField.toString()+" is not present");
				attempts = 3;
				// takeScreenShot(elementName);
			}
		}
		return toRet;
	}
	public boolean waitForJSandJQueryToLoad() {

		WebDriverWait wait = new WebDriverWait(driver, 30);

		// wait for jQuery to load
		ExpectedCondition<Boolean> jQueryLoad = new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver driver) {
				try {
					return ((Long) ((JavascriptExecutor) driver).executeScript("return jQuery.active") == 0);
				} catch (Exception e) {
					// no jQuery present
					return true;
				}
			}
		};

		// wait for Javascript to load
		ExpectedCondition<Boolean> jsLoad = new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver driver) {
				return ((JavascriptExecutor) driver).executeScript("return document.readyState").toString()
						.equals("complete");
			}
		};

		return wait.until(jQueryLoad) && wait.until(jsLoad);
	}

	
	@Override
	public boolean alertPopupVerification()
	{ 
		    try 
		    { 
		        driver.switchTo().alert(); 
		        System.out.println(driver.switchTo().alert().getText());
		        
		        
		        return true; 
		    }    
		    catch (Exception e) 
		    { 
		    	//takeScreenShot("Chrome popup");
		        return false; 
		    }     
	}
	
	
	@Override
	public boolean waitforAlertPopup() 
	{
		boolean valid = false;
		try
		{
			 int i=0;
			   while(i++<5)
			   {
			        try
			        {
			            Alert alert = driver.switchTo().alert();
			            valid = true;
			            break;
			        }
			        catch(NoAlertPresentException e)
			        {
				        Thread.sleep(1000);
				        valid = false;
				        continue;
			        }
			   }
		}
		catch (Exception e)
		{
			//takeScreenShot("Chrome popup");
			valid = false;
		}
		
		return valid;
	}
	
	@Override
	public String alertPopupGetText()
	{
		String text = "";
			    try 
			    { 
			        driver.switchTo().alert(); 
			        text = (driver.switchTo().alert().getText());
			    }    
			    catch (Exception e) 
			    { 
			    	//takeScreenShot("Chrome popup");
			    }     
	    return text; 
	}

	@Override
	public boolean alertPopupAccept()
	{ 
		    try 
		    { 
		        driver.switchTo().alert().accept();; 
		        return true; 
		    }    
		    catch (Exception e) 
		    { 
		    	//takeScreenShot("Chrome popup");
		        return false; 
		    }     
	}	 
	
	@Override
	public boolean explicitWaitforClickable(By path,String elementName)  
	{
		try
		{
	        wait.ignoring(StaleElementReferenceException.class)
	        .until(ExpectedConditions.elementToBeClickable(path));
		/*	
			WebDriverWait wait = new WebDriverWait(this.driver,seconds).ignoring(StaleElementReferenceException.class);
			wait.until(ExpectedConditions.elementToBeClickable(path));*/
			return true;
		}
		catch(Exception e)
		{
			loggerUtils.stackTracePrint(e);
			ExtentTestManager.getTest().log(LogStatus.INFO, getStackTrace(e));
			ExtentTestManager.getTest().log(LogStatus.INFO,elementName+" xpath "+path.toString()+" is not present");
			//takeScreenShot(elementName);
			return false;
		}
	}
	
	@Override
	public boolean SwitchtoIframe(By path,String elementName)
	{
		try
		{
			/*WebDriverWait wait = new WebDriverWait(this.driver,seconds);
			wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(path));*/
	        wait.ignoring(StaleElementReferenceException.class)
	        .until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(path));
			return true;
		}
		catch(Exception e)
		{
			loggerUtils.stackTracePrint(e);
			ExtentTestManager.getTest().log(LogStatus.INFO, getStackTrace(e));
			ExtentTestManager.getTest().log(LogStatus.INFO,elementName+" xpath "+path.toString()+" is not present");
			//takeScreenShot(elementName);
			return false;
		}
	}
	
	@Override
	public boolean SwitchtoParentFrame(String elementName)
	{
		try
		{
			driver.switchTo().parentFrame();
			return true;
		}
		catch(Exception e)
		{
			//BaseClass.processErrorCodes(e,elementName);
			loggerUtils.stackTracePrint(e);
			//takeScreenShot(elementName);
			return false;
		}
	}
	
	
	
	@Override
	public boolean waitForPageLoad(By path, String elementName, String style)
	{
		try
		{
			
	        wait.ignoring(StaleElementReferenceException.class)
			.until(ExpectedConditions.attributeContains(path, "style", style));
			return true;		
		}
		catch(Exception e)
		{
			e.printStackTrace();
            //takeScreenShot(elementName);
			return false;
		}
	}
	

	
	@Override
	public boolean clearText(By path, String elementName)
	{
		Boolean valid = false;
		try
		{
			if(explicitWaitforVisibility(path,elementName))
			 {
				WebElement element  = driver.findElement(path);
				element.clear();
				valid  = true;
			 }
		}
		catch(Exception e)
		{
			//BaseClass.processErrorCodes(e,elementName+" ("+path.toString()+")");
			loggerUtils.stackTracePrint(e);
            //takeScreenShot(elementName);
            
    	}
		
		return valid;
	}
	
	@Override
	public String getCookie(String cookieName)
	{
		String CookieValue="";
		try
		{
			CookieValue=driver.manage().getCookieNamed(cookieName).getValue();
		}
		catch(Exception e)
		{
			CookieValue="NOTFOUND";
		}
		return CookieValue;
	}
	
		
	@Override
	public boolean navigateWithKeys(Keys value, int count, String elementName)
	{
		Boolean valid = false;
		try
		{
			Actions act = new Actions(this.driver);
			for(int i = 0; i<count; i++)
			{
				act.sendKeys(value).build().perform();
			}
			valid = true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
            //takeScreenShot(elementName);
            
    	}
		return valid;
	}

	@Override
	public boolean waitForScriptLoad()
	{
		boolean valid = false;
		try
		{
			driver.manage().timeouts().setScriptTimeout(Wait_Time, TimeUnit.SECONDS);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return valid;
	}

	@Override
	public String takeScreenShotReturnFilePath() 
	{
		String scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BASE64);
		return "data:image/jpg;base64," + scrFile;
	}
	
	@Override
	public boolean changeFocus(int tabNumber)
	{
		boolean changeFocus = false;
		try
		{
			wait.until(ExpectedConditions.numberOfWindowsToBe(2));
			tabs = new ArrayList<String> (driver.getWindowHandles());
		    driver.switchTo().window(tabs.get(tabNumber));
		    changeFocus = true;
		}
		catch (Exception e)
		{
			loggerUtils.stackTracePrint(e);
			//takeScreenShot("Tab focus");
		}
		return changeFocus;
	}
	
	@Override
	public void changeFocusToDefaultTab()
	{
		try
		{
		    driver.close();
		    driver.switchTo().window(tabs.get(0));
		}
		catch (Exception e)
		{
			loggerUtils.stackTracePrint(e);
			//takeScreenShot("Tab focus to default");
		}
	}
	
	@Override
	public void driverMode(String view)
	{
		if(view.equalsIgnoreCase("mobile"))
		{
			driver.manage().window().setSize(new Dimension(500,650));
		}
		else if(view.equalsIgnoreCase("tablet"))
		{
			driver.manage().window().setSize(new Dimension(1000,650));
		}
	}

	@Override
	public String getPageSource()
	{
		return driver.getPageSource();
	}

	@Override
	public void setProxyDomain(String domain)
	{
		  if(BaseClass.properties.getProperty("RunWithProxy").equalsIgnoreCase("YES"))
          {
			  proxy.newHar(domain);
          }
	}
	
	@Override
	public void enableHarCaptureTypes()
	{	
		if(BaseClass.properties.getProperty("RunWithProxy").equalsIgnoreCase("YES"))
    	{
			proxy.enableHarCaptureTypes(CaptureType.REQUEST_CONTENT, CaptureType.RESPONSE_HEADERS);
    	}
	}
	
	@Override
	public Har getHar()
	{
		Har har = null;
		if(BaseClass.properties.getProperty("RunWithProxy").equalsIgnoreCase("YES"))
        {		
			har=proxy.getHar();
        }
		return har;
	}
	
	@Override
	public void abortProxy()
	{
		if(BaseClass.properties.getProperty("RunWithProxy").equalsIgnoreCase("YES"))
        {
			proxy.abort();
        }
	}
	
	@Override
	public void saveNetworkLog(String fileName)
	{
		if(BaseClass.properties.getProperty("RunWithProxy").equalsIgnoreCase("YES"))
        {
			File files = new File(BaseClass.currentRunReportPath+"\\networkLogs");
			files.mkdirs();
			String filePath=BaseClass.currentRunReportPath+"\\networkLogs\\"+fileName;
			Har har=proxy.getHar();
			File harFile = new File(filePath);
			try {
				har.writeTo(harFile);
			} catch (IOException ex) {
				 ex.printStackTrace();
			}
        }
	}

	@Override
	public String getElementAttributeWithoutVisibility(By path,String attrName)
	{
		String text = "";
		try
		{
			WebElement element = this.driver.findElement(path);
			text = element.getAttribute(attrName);
		}
		catch(Exception e)
		{
			
			e.printStackTrace();
		}
		return text;
	}
	

	public void pageReload()
	{
		try
		{
			driver.navigate().refresh();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	@Override
	public String getPseudoValue(By path, String pseudoType, String elementName)
	{
		String pseudoValue = "";
		try
		{
			WebElement element = this.driver.findElement(path);
			pseudoValue = ((JavascriptExecutor)driver)
			        .executeScript("return window.getComputedStyle(arguments[0], ':"+pseudoType+"').getPropertyValue('content');",element).toString();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return pseudoValue;
	}
	
	@Override
	public boolean switchToWidow() 
	{
		boolean valid = false;
		try
		{
			
	        wait.ignoring(StaleElementReferenceException.class)
			.until(ExpectedConditions.numberOfWindowsToBe(2));
			Set<String>ids= driver.getWindowHandles();
			Iterator<String> it =ids.iterator();
			String parentId=it.next();
			//System.out.println(parentId);
			String childId=it.next();
			driver.switchTo().window(childId);
			//System.out.println(childId);
			valid = true;
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			valid = false;
		}
		return valid;
	}

	@Override
	public String jsQueryExecutorById(By path)
	{
		return (String) ((JavascriptExecutor) this.driver).executeScript("return document.getElementById('"+path.toString().replace("By.id: ", "")+"').value");
	}	

	@Override
	public void  navigateBack() 
	{
		driver.navigate().back();
	}
	
	@Override
	public boolean selectByValue(By path,String value,String elementName)
	{
		Boolean valid = false;
		try
		{	
			WebElement element = this.driver.findElement(path);
			Select dropdown= new Select(element);
			dropdown.selectByValue(value);
			valid = true;
		}
		catch(Exception e)
		{
			//BaseClass.processErrorCodes(e,elementName+" ("+path.toString()+")");
			loggerUtils.stackTracePrint(e);
			loggerUtils.failLog(elementName+"is not present");
			//takeScreenShot(elementName);
		}
		return valid;
	}
	
	@Override
	public String jsQueryExecutorByClass(By path)
	{	
		return (String) ((JavascriptExecutor) this.driver).executeScript("return document.getElementsByClassName('"+path.toString().replace("By.className: ", "")+"')[0].value");
	}

	@Override
	public boolean compareElementTextWithIgnoreCase(By path,String expectedText,String elementName)
	{
		Boolean valid = false;
			try
			{
				if(explicitWaitforVisibility(path,elementName))
				{
					scrollToElement(path, elementName);
					String currentText = getText(path, elementName).trim();
					expectedText=Jsoup.parse(expectedText).text();
					if(currentText.equalsIgnoreCase(expectedText))
					{
						loggerUtils.passLog(elementName+" is verified");
						valid = true;
					}
					else
					{
						//Logging
						loggerUtils.failLog(elementName+" is mismatched");
					}
				}
			}
			catch(Exception e)
			{
				loggerUtils.stackTracePrint(e);
				loggerUtils.failLog(elementName+" is not present");
				//takeScreenShot(elementName);
			}
			return valid;
	}
	
	@Override
	public boolean setWindowSize(int width, int height)
	{
		Boolean valid = false;
		try
		{
			driver.manage().window().setSize(new Dimension(width, height));
			valid = true;
		}
		catch (Exception e) 
		{
			loggerUtils.stackTracePrint(e);
			e.printStackTrace();
		}
		return valid;
	}
	
	@Override
	public void deleteAllCookies()
	{
//		Cookie name = driver.manage().getCookieNamed("x-sk-session-id");
//		Set<Cookie> allCookies = driver.manage().getCookies();
//		for (Cookie cookie : allCookies) 
//		{
//			//System.out.println(cookie.getName());
//		    driver.manage().deleteCookieNamed(cookie.getName());
//		}
//		
//		driver.manage().deleteAllCookies();
//		driver.manage().deleteCookieNamed("JSESSIONID");
//		driver.manage().deleteCookieNamed("x-sk-session-id " );
		
		Cookie logout = new Cookie("x-sk-session-id","ca2722e94c6146065087ad3cc5ba846e","int.skavacommerce.com",null,null);
		
		driver.manage().addCookie(logout);

	}
	
   @Override
	public void PageEnd() 
	{
		((JavascriptExecutor) driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
	}

	@Override
	public boolean moveToElement(By path, String elementName)
	{
		Boolean valid = false;
		try
		{
			WebElement element = driver.findElement(path);
			Actions act = new Actions(this.driver);
			act.moveToElement(element).build().perform();
			valid = true;
		}
		catch(Exception e)
		{
			//BaseClass.processErrorCodes(e,elementName+" ("+path.toString()+")");
			loggerUtils.stackTracePrint(e);
            //takeScreenShot(elementName);   
    	}
		return valid;
	}
	
	@Override
	 public void checkLayout(String spec, String testCaseName) 
	{
		LayoutReport layoutReport = null;
			try
			{
				layoutReport=Galen.checkLayout((WebDriver) this.driver,spec, Arrays.asList("mobile"));
		
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
	}

	@Override
    public Map<String, String> getBrowserDetails() 
    {
        Map<String, String> browserDetails = new HashMap<String, String>();
        try 
        {
            Capabilities cap = ((RemoteWebDriver) this.driver).getCapabilities();
            String browserName = cap.getBrowserName().toUpperCase();
            browserDetails.put("Browser Name", browserName);
            String browserVersion = cap.getVersion();
            browserDetails.put("Browser Version", browserVersion);
            browserDetails.put("Port", String.valueOf(port));
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        return browserDetails;

    }
	
	@Override
    public void driverSize(int length , int breadth) 
	{
        driver.manage().window().setSize(new Dimension(length, breadth));
    }

	@Override
    public boolean mouseHoverElement(By buttonField,String elementName)
    {
        Boolean valid = false;
        try
        {
            if(explicitWaitforVisibility(buttonField,elementName))
            {
                Actions act=new Actions(driver);
                act.moveToElement(driver.findElement(buttonField)).build().perform();
                valid = true;
                loggerUtils.passLog(elementName+" is hovered");
            }
            else
            {
                ExtentTestManager.getTest().log(LogStatus.INFO,elementName+" xpath "+buttonField.toString()+" is not visible");
            }
        }
        catch (Exception e)
        {
            loggerUtils.stackTracePrint(e);
            ExtentTestManager.getTest().log(LogStatus.INFO,elementName+" xpath "+buttonField.toString()+" is not present");
            loggerUtils.failLog(elementName+"is not present");
            //takeScreenShot(elementName);
        }
        return valid;
    }
	
	@Override
    public boolean waitForElementAttribute(By path,String attribute, String value, String elementName) 
    {
    	 Boolean valid = false;
    	 try 
    	 {
    		WebElement element = driver.findElement(path);
            wait.until(ExpectedConditions.attributeContains(element, attribute, value));
            valid = true;
         } 
    	 catch (Exception e) 
    	 {
             //BaseClass.processErrorCodes(e,elementName+" ("+path.toString()+")");
             loggerUtils.stackTracePrint(e);
             //takeScreenShot(elementName);
         }
    	 return valid;
    }

	@Override
	public void takeScreenshotWithPath(String path, String elementName) 
	{
		try
	      {
	    	  DateFormat dateFormat = new SimpleDateFormat("h_m_s");
			  Date date = new Date();
	    	  TakesScreenshot scrShot =((TakesScreenshot)this.driver);
	    	  File sourceFile=scrShot.getScreenshotAs(OutputType.FILE);
	    	  FileUtils.copyFile(sourceFile,new File(path+elementName+"_"+dateFormat.format(date)+".png"));
	      }
	      catch (Exception e)
	      {
	    	  //BaseClass.processErrorCodes(e,path);
	    	  loggerUtils.stackTracePrint(e);
	      }
	}
	
	@Override
	public WebElement element(By path)
	{
		return driver.findElement(path);
	}
	
	@Override
	public boolean actionClickElement(By path,String elementName)
	{
		Boolean valid = false;
		try
		{
			Actions click = new Actions(this.driver);
			WebElement webPath = driver.findElement(path);
			click.moveToElement(webPath).click().build().perform();
			valid = true;
			loggerUtils.passLog(elementName+" is clicked");
		}	
		catch (Exception e) 
		{
			loggerUtils.stackTracePrint(e);
			ExtentTestManager.getTest().log(LogStatus.INFO,elementName+" xpath "+path.toString()+" is not present");
			loggerUtils.failLog(elementName+"is not present");
			takeScreenShot(elementName);
		}
		return valid;
	}
	
	
	//Paramesh
	@Override
	public boolean explicitWaitforInvisibilityWithoutLocating(By path,int seconds,String elementName)
	{
		try
		{
			WebDriverWait wait = new WebDriverWait(this.driver,seconds);
			wait.until(ExpectedConditions.invisibilityOf(driver.findElement(path)));
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			takeScreenShot(elementName);
			return false;
		}
	}
	
	@Override
	public boolean actionEnterText(By textField, String textValue,String elementName)
	{
		Boolean valid=false;
		try
		{
			Actions actions = new Actions(driver);
			WebElement path = driver.findElement(textField);
			actions.moveToElement(path);
			actions.click();
			actions.sendKeys(textValue);
			actions.build().perform();
			valid=true;
			loggerUtils.passLog(elementName+" is entered in the field");
		}
		catch (Exception e) 
		{
			 loggerUtils.stackTracePrint(e);
			 ExtentTestManager.getTest().log(LogStatus.INFO,elementName+" xpath "+textField.toString()+" is not present");
			 loggerUtils.failLog(elementName+"is not present");
			 takeScreenShot(elementName);
		}
		
		return valid;
	}
	
	@Override
	public void actionTabKey(By textField,String elementName)
	{
		try
		{
			Actions actions = new Actions(driver);
			WebElement path = driver.findElement(textField);
			actions.moveToElement(path);
			actions.click();
			actions.sendKeys(Keys.TAB);
			actions.build().perform();
			loggerUtils.passLog(elementName+" tab key pressed");
		}
		catch (Exception e) 
		{
			 loggerUtils.stackTracePrint(e);
			 ExtentTestManager.getTest().log(LogStatus.INFO,elementName+" xpath "+textField.toString()+" is not present");
			 loggerUtils.failLog(elementName+" tab key not pressed");
			 takeScreenShot(elementName);
		}
	}
	
	
	 @Override
	    public void draganddrop(By fromPath,By toPath,String elementName)
	    {
	        Actions act=new Actions(driver);
	        act.dragAndDrop(driver.findElement(fromPath), driver.findElement(toPath)).build().perform();
	        
//	       
//	        
	        WebElement LocatorFrom = driver.findElement(fromPath);
	        WebElement LocatorTo = driver.findElement(toPath);
	        String xto=Integer.toString(LocatorTo.getLocation().x);
	        String yto=Integer.toString(LocatorTo.getLocation().y);
	        ((JavascriptExecutor)driver).executeScript("function simulate(f,c,d,e){var b,a=null;for(b in eventMatchers)if(eventMatchers[b].test(c)){a=b;break}if(!a)return!1;document.createEvent?(b=document.createEvent(a),a==\"HTMLEvents\"?b.initEvent(c,!0,!0):b.initMouseEvent(c,!0,!0,document.defaultView,0,d,e,d,e,!1,!1,!1,!1,0,null),f.dispatchEvent(b)):(a=document.createEventObject(),a.detail=0,a.screenX=d,a.screenY=e,a.clientX=d,a.clientY=e,a.ctrlKey=!1,a.altKey=!1,a.shiftKey=!1,a.metaKey=!1,a.button=1,f.fireEvent(\"on\"+c,a));return!0} var eventMatchers={HTMLEvents:/^(?:load|unload|abort|error|select|change|submit|reset|focus|blur|resize|scroll)$/,MouseEvents:/^(?:click|dblclick|mouse(?:down|up|over|move|out))$/}; " +
	        "simulate(arguments[0],\"mousedown\",0,0); simulate(arguments[0],\"mousemove\",arguments[1],arguments[2]); simulate(arguments[0],\"mouseup\",arguments[1],arguments[2]); ",
	        LocatorFrom,xto,yto);
	       
	        Point point = driver.findElement(toPath).getLocation();
	        int x=point.getX();
	        int y=point.getY();
	        act.dragAndDropBy(driver.findElement(fromPath), x, y).build().perform();
	    }

	@Override
	public String generateRandomString(int length) 
	{
		String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		StringBuilder salt = new StringBuilder();
		Random rnd = new Random();
		while (salt.length() < length) { // length of the random string.
			int index = (int) (rnd.nextFloat() * SALTCHARS.length());
			salt.append(SALTCHARS.charAt(index));
			}
			String saltStr = salt.toString();
			return saltStr;
	}
	
	

	public void refreshPage()
	{
		try
		{
			driver.navigate().refresh();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	@Override
	public String hiddenGetText(By path,String elementName)
	{
		String text = "";
		try
		{
			
				WebElement element = driver.findElement(path);
				String script = "return arguments[0].innerText";
				text = (String) ((JavascriptExecutor) driver).executeScript(script, element);
			
		}
		catch(Exception e)
		{
			//BaseClass.processErrorCodes(e,elementName+" ("+path.toString()+")");
			loggerUtils.stackTracePrint(e);
			loggerUtils.failLog(elementName+" is not present");
			//takeScreenShot(elementName);
		}
		return text;
		
	}
	
	public static void setFlag()
	{
		System.setProperty("continueExecution","false");
		
		System.out.println(System.getProperty("continueExecution"));
	}
	
	 public String getSessionId() {
	      return ((RemoteWebDriver) this.driver).getSessionId().toString() + ", " + Thread.currentThread().getName();
	  }
		
		private String getStackTrace(Exception e) {			
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			return sw.toString();
			
		}
		
		@Override
		public void escape() {
	        Actions action = new Actions(driver);
	        action.sendKeys(Keys.ESCAPE).build().perform();
	    }

		@Override
		public void draganddrop(WebElement from, WebElement to, String elementName) {
//			  Actions act=new Actions(driver);
//		        act.dragAndDrop(from, to).build().perform();
//		        act.clickAndHold(from).moveToElement(to).release().build().perform();
			  Actions builder = new Actions(driver);
//			  int x2 =to.getLocation().getX();
//			  int y2 = to.getLocation().getY();
//			  builder.clickAndHold(from);
//			  builder.moveByOffset(x2,y2);
//			  builder.moveToElement(to);
//			  builder.release();
//			  builder.build().perform();
			/*  IAction dragAndDrop=builder.ClickAndHold(from).MoveToElement(to).Release(to).Build();
			dragAndDrop.Perform();*/
			  Action dragAndDrop = builder.clickAndHold(from).moveToElement(to).release(to).build();
//					  Thread.sleep(2000);
					  dragAndDrop.perform();
//			  builder.dragAndDropBy(from, x2, y2).build().perform();;
			  builder.clickAndHold(from).pause(2000).moveToElement(to).release().build().perform();
			  
			  Point coordinates1 = from.getLocation();
			  Point coordinates2 = to.getLocation();  
			  Robot robot;
			try {
				robot = new Robot();
				  robot.mouseMove(coordinates1.getX(), coordinates1.getY());
				  robot.mousePress(InputEvent.BUTTON1_MASK);
				  robot.mouseMove(coordinates2.getX(), coordinates2.getY());
				  robot.mouseRelease(InputEvent.BUTTON1_MASK);
			} catch (AWTException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}           
			
//		       
//		        
//		        WebElement LocatorFrom = driver.findElement(fromPath);
//		        WebElement LocatorTo = driver.findElement(toPath);
//		        String xto=Integer.toString(LocatorTo.getLocation().x);
//		        String yto=Integer.toString(LocatorTo.getLocation().y);
//		        ((JavascriptExecutor)driver).executeScript("function simulate(f,c,d,e){var b,a=null;for(b in eventMatchers)if(eventMatchers[b].test(c)){a=b;break}if(!a)return!1;document.createEvent?(b=document.createEvent(a),a==\"HTMLEvents\"?b.initEvent(c,!0,!0):b.initMouseEvent(c,!0,!0,document.defaultView,0,d,e,d,e,!1,!1,!1,!1,0,null),f.dispatchEvent(b)):(a=document.createEventObject(),a.detail=0,a.screenX=d,a.screenY=e,a.clientX=d,a.clientY=e,a.ctrlKey=!1,a.altKey=!1,a.shiftKey=!1,a.metaKey=!1,a.button=1,f.fireEvent(\"on\"+c,a));return!0} var eventMatchers={HTMLEvents:/^(?:load|unload|abort|error|select|change|submit|reset|focus|blur|resize|scroll)$/,MouseEvents:/^(?:click|dblclick|mouse(?:down|up|over|move|out))$/}; " +
//		        "simulate(arguments[0],\"mousedown\",0,0); simulate(arguments[0],\"mousemove\",arguments[1],arguments[2]); simulate(arguments[0],\"mouseup\",arguments[1],arguments[2]); ",
//		        LocatorFrom,xto,yto);
		       
//		        Point point = driver.findElement(to).getLocation();
//		        int x=point.getX();
//		        int y=point.getY();
//		        act.dragAndDropBy(driver.findElement(from), x, y).build().perform();
//			// TODO Auto-generated method stub
			
		}
		@Override
		public boolean PressEnterKey(By textField,String elementName)
		{
			Boolean valid = false;
			 try
			 {
				Boolean visibility = explicitWaitforVisibility(textField,elementName);
				if( visibility)
				{
					WebElement element = this.driver.findElement(textField);
					element.sendKeys(Keys.ENTER);
					valid = true;
					loggerUtils.passLog(elementName+" Press ENTER key in the field");
				}
			 }
			 catch(Exception e)
			 {
				 //BaseClass.processErrorCodes(e,elementName+" ("+textField.toString()+")");
				 loggerUtils.stackTracePrint(e);
				 loggerUtils.failLog(elementName+"is not present");
				 //takeScreenShot(elementName);
			 }
			 return valid;
		}
		
		@Override
		   public void jsQueryEnterText( By path,String text)
		   {
		        JavascriptExecutor js = (JavascriptExecutor)driver;
		           String val = text;
		            WebElement element = driver.findElement(path);
		            element.clear();
		            js.executeScript("arguments[0].value = '';", element);
		            for (int i = 0; i < val.length(); i++){
		                char c = val.charAt(i);
		                String s = new StringBuilder().append(c).toString();
		                element.sendKeys(s);
		            }
		   }
}
