package com.skava.object.repository;

import org.openqa.selenium.By;

public class addtocart {
	
	public static final By plpproduct1 =By.xpath("(//div[@class='product--list__item__image'])[1]") ;
	//public static final By plpproduct2 =By.xpath("(//div[@class='flex-grid__item product--list--hover'])[1]");
	
	/***pdp**/
	public static final By pdpproductdesc =By.xpath("//h1[@class='product-details__description__brand']");
	
	public static final By pdpdescription =By.xpath("//small[@class='product-details__description__name text__weight--normal']");
	public static final By welcometext =By.xpath("//span[@class='account__firstname']");
	public static final By addtocart1=By.xpath("(//div[@class='header__icon'])[4]");
	public static final By addtocart=By.xpath("//div[@class='btn btn--bigger-icon preventspinner btn-default']");
	/***cart button in cart page**/
	public static final By addtocartbuttonvalidate1=By.xpath("//span[@class='icon icon--till']");
	public static final By cartelementvalidate=By.xpath("(//div[@class='cart-item--name'])[1]");
	public static final By clearcart=By.xpath("//i[@class='icon icon--inline icon--trash-can']");
	
	
}
